import {UserManagerSettings, WebStorageStateStore} from 'oidc-client';

export function getClientSettings(): UserManagerSettings {
  return {
    authority: 'https://steamcommunity.com/openid',
    client_id: 'angular_spa',
    redirect_uri: 'https://localhost:4200/callback',
    post_logout_redirect_uri: 'https://localhost:4200/',
    response_type: 'id_token token',
    scope: 'openid profile api1',
    filterProtocolClaims: true,
    loadUserInfo: true
  };
}

// userStore: new WebStorageStateStore({ store: window.localStorage })
